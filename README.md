# Seafile SQLite

This project configures the official Seafile Docker image [seafileltd/seafile-mc](https://hub.docker.com/r/seafileltd/seafile-mc) to use SQLite instead of a "real" database.

Changes to official image:

- installation of sqlite3
- bypass of setup-seafile-mysql.sh and wait_for_mysql() in the container scripts

## Usage

You can build the container yourself using the Dockerfile, or use the prebuilt image with `docker pull registry.gitlab.com/jnaskali/seafile-sqlite`.

An example docker-compose.yaml file is included, which includes memcached and caddy ingress. You can test the install by populating the .env file with your details and running Seafile with `docker compose up -d`.
